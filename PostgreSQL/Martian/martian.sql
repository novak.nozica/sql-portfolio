-- PostgreSQL Portfolio Project 
-- Author: Novak Nozica Junior Data Analyst

-- Welcome to the future! 

-- It is a year 2056 and we have colonies throught solar system. Mars is now tourist destination.
-- Today we are going to generate several reports for monitoring martian activity.
-- One thing that might suprise you about the future is that everyone still uses SQL! :)

-- Database martian contains five tables (martian,base,visitor,inventory,supply)

-- The martian table is a list of all people living on the Red Planet. After all,if you are living on Mars, you are no longer Earthling - you are Martian.
-- The martian table contains (martian_id, first_name, last_name, base_id, super_id)

-- The base table holds information on all different habitats on planet Mars
-- (base_id, base_name, founded, ticket_price)

-- Mars is now tourist destination. Every visitor to Mars is tracked in the visitor table
-- (visitor_id, host_id, first_name, last_name, email, country)

-- The inventory table keeps track of the supplies at each base
-- (base_id, supply_id, quantity, date)

-- The supply table shows what is available at the central Martian distribution center
-- (supply_id, name, description, quantity)

CREATE DATABASE martian;

-- Creating tables

CREATE TABLE martian(
martian_id INT PRIMARY KEY,
first_name VARCHAR(20),
last_name VARCHAR(20),
base_id INT NOT NULL,
super_id INT 
);

CREATE TABLE base(
base_id INT PRIMARY KEY,
base_name VARCHAR(20),
founded DATE,
ticket_price INT
);

CREATE TABLE visitor(
visitor_id INT PRIMARY KEY,
host_id INT,
first_name VARCHAR(20),
last_name VARCHAR(20),
email VARCHAR(20),
country VARCHAR (20)
);

CREATE TABLE inventory(
base_id INT,
supply_id INT,
quantity INT,
date DATE
);

CREATE TABLE supply(
supply_id INT PRIMARY KEY,
name VARCHAR (20) UNIQUE,
description TEXT,
quantity INT
);

-- Adding foreign keys 

ALTER TABLE martian
ADD FOREIGN KEY (base_id) REFERENCES base (base_id),
ADD FOREIGN KEY (super_id) REFERENCES martian (martian_id);

ALTER TABLE visitor
ADD FOREIGN KEY (host_id) REFERENCES martian (martian_id);

ALTER TABLE inventory
ADD FOREIGN KEY (base_id) REFERENCES base (base_id),
ADD FOREIGN KEY (supply_id) REFERENCES supply (supply_id);

-- Adding values to the tables

INSERT INTO martian
VALUES 
(1,'May','Murray',1,NULL),
(2,'Zoya','Joseph',2,NULL),
(3,'Jakub','Cotton',3,NULL),
(4,'Malaika','Wu',4,NULL),
(5,'Omari','Mullen',1,1),
(6,'Karen','Knight',2,2),
(7,'Honor','Haines',3,3),
(8,'Randy','Craig',4,4),
(9,'Zaara','Stevens',1,1),
(10,'KimBetty','Johnston',2,2),
(11,'Stacey','Kim',3,3),
(12,'Rohan','Taylor',4,4),
(13,'Mason','Griffith',1,1),
(14,'Pauline','Farmer',2,2),
(15,'Conrad','Mathis',3,3),
(16,'Xanthe','Mcgrath',4,4),
(17,'Annika','Riddle',1,1),
(18,'Aleena','Cannon',2,2);

INSERT INTO base
VALUES 
(1,'Atlas','2047-07-07',15700),
(2,'Saga','2049-01-01',13200),
(3,'Themis','2050-05-05',22500),
(4, 'Genesis Station','2051-07-08',12500),
(5,'Valhalla Station',NULL,25000);

INSERT INTO visitor
VALUES 
(1,1,'Novak','Nozica','noconoco@gmail.com','Serbia'),
(2,1,'Ronald','Reed','rr21@gmail.com','USA'),
(3,2,'Nastya','Vorobeva','navor@gmail.com','Russia'),
(4,2,'Joanna','Martinez','martinez@yahoo.com','USA'),
(5,3,'Jerzy','Kokot','kokot@gmail.com','Poland'),
(6,3,'Priscila','Mancillas','pris@yahoo.com','Brazil'),
(7,4,'Antonio','Galeas','antonio@gmail.com','Argentina'),
(8,4,'Gerlach','Grosse',NULL,'Germany'),
(9,1,'Leonore','Schwarz','schwarz@gmail.com','Germany'),
(10,2,'Sylvestre','Fournier','sy@yahoo.com','France'),
(11,3,'Romano','Moretti','romano@gmail.com','Italy'),
(12,4,'Milos','Jankovic','milosj@gmail.com','Serbia'),
(13,NULL,'Nataliya','Lebedeva',NULL,'Russia'),
(14,NULL,'Peng','Tsai','peng@yahoo.com','China'),
(15,1,'Kong','Shao','kong@gmail.com','China'),
(16,1,'Kushal','Din','din@gmail.com','India'),
(17,2,'Jelena','Markovic','jeca@hotmail.com','Serbia'),
(18,2,'Naum','Vasiliev',NULL,'Russia'),
(19,3,'Ismael','Chase','ismail@gmail.com','USA'),
(20,3,'Constantin','Lebeau',NULL,'France'),
(21,4,'Shannon','McClendon','mc@hotmail.com','Canada'),
(22,4,'Tracy','Laurel',NULL,'Australia'),
(23,NULL,'Apurva','Suresh','suresh@gmail.com','India'),
(24,1,'Amala','Ziegler','amala@hotmail.com','Germany'),
(25,2,'Elaine','Delacroix','dela@gmail.com','France'),
(26,3,'Lorena','Rosen','lori@gmail.com','USA');

INSERT INTO supply
VALUES 
(1,'Solar Panel','Standard 1x1 meter cell', 812),
(2,'Water Filter','This takes things out of your water so it is drinkable', 414),
(3,'Water Gallon','A standard glass contains eight ounces of water', 8518),
(4,'Air Filter','Removes 99% of all Martian dust from your ventilation unit', 217),
(5,'Mars Bars','The original nutrient bar made with the finest bioengineered ingredients', 8721),
(6,'Batery Cell','Batery cell for power grid', 14),
(7,'Frozen Pizza','The Martian favorite food', 2618);

INSERT INTO inventory
VALUES 
(1,7,40,'2056-01-01'),
(2,2,16,'2056-01-01'),
(2,5,75,'2056-01-02'),
(3,5,22,'2056-01-06'),
(4,1,2,'2056-01-07'),
(4,3,50,'2056-01-09'),
(3,1,10,'2056-01-14'),
(1,6,2,'2056-01-22'),
(2,3,14,'2056-01-29'),
(4,7,67,'2056-02-01'),
(3,2,10,'2056-02-01'),
(1,4,15,'2056-02-04'),
(2,7,100,'2056-02-06'),
(1,2,17,'2056-02-12'),
(2,4,12,'2056-02-18'),
(3,4,18,'2056-02-19'),
(4,5,25,'2056-02-20'),
(4,1,5,'2056-02-24');

-- The Valhalla base station is finally opened, so we need to update the base table

UPDATE base
SET founded = '2056-03-01'
WHERE base_id = 5;

-- The Valhalla base station has an increased need for supplies due to the opening

CREATE PROCEDURE valhalla_inventory (base_id INT, supply_id INT, quantity INT, date DATE)
LANGUAGE 'plpgsql'
AS $$
BEGIN
INSERT INTO inventory VALUES (base_id,supply_id,quantity,date);
COMMIT;
END;
$$;

CALL valhalla_inventory(5,7,100,'2056-03-01');

-- Create report of Martian names and their bases

SELECT
	m.martian_id,
	m.first_name,
	m.last_name,
	b.base_name
FROM martian m
RIGHT JOIN base b
	ON m.base_id = b.base_id
ORDER BY b.base_name;

-- Which base is the oldest and which has the most expensive tickets

SELECT *
FROM base
WHERE ticket_price = 
	(SELECT MAX(ticket_price) FROM base);
	
SELECT *
FROM base
ORDER BY founded
LIMIT 1;

-- Display name of visitor's host

SELECT 
	CONCAT(m.first_name,' ',m.last_name) AS host,
	CONCAT(v.first_name,' ',v.last_name) AS visitor
FROM martian m
RIGHT JOIN visitor v
	ON m.martian_id = v.host_id
ORDER BY martian_id;

-- We see that 3 visitors don't have a host

SELECT *
FROM visitor
WHERE host_id IS NULL;

UPDATE visitor
SET host_id = 2
WHERE visitor_id IN(13,14,23);

-- Inventory for Atlas base

SELECT 
	s.supply_id,
	COALESCE(i.quantity, 0),
	s.name,
	s.description
FROM 
	(SELECT * FROM inventory WHERE base_id = 1) AS i
RIGHT JOIN supply s
	ON i.supply_id = s.supply_id
ORDER BY supply_id;
	
-- Display list of each Martian and the person they report to

SELECT
	CONCAT(m.first_name, ' ',m.last_name) AS martian,
	CONCAT(s.first_name, ' ',s.last_name) AS reports_to
FROM martian m
LEFT JOIN martian s
	ON m.super_id = s.martian_id
ORDER BY m.martian_id;

-- From which countries do most visitors come

SELECT
	country,
	COUNT(visitor_id) AS number_of_visitors
FROM visitor
GROUP BY country
ORDER BY number_of_visitors DESC;

-- Create View off all people on Mars 

CREATE VIEW people_on_mars AS 
SELECT CONCAT('m',martian_id), first_name, last_name, 'Martian' AS status
FROM martian
	UNION
SELECT CONCAT('v',visitor_id), first_name, last_name, 'Visitor' AS status
FROM visitor;

-- Supply quantities at each 

SELECT
	b.base_id,
	s.supply_id,
	s.name,
		COALESCE(
		(SELECT quantity FROM inventory
		WHERE base_id = b.base_id AND supply_id = s.supply_id LIMIT 1),0) AS quantity
FROM base b
CROSS JOIN supply s;

-- What supplies are missing from each base

SELECT
	b.base_id,
	s.supply_id,
	s.name,
		COALESCE(
		(SELECT quantity FROM inventory
		WHERE base_id = b.base_id AND supply_id = s.supply_id LIMIT 1),0) AS quantity
FROM base b
CROSS JOIN supply s
GROUP BY b.base_id,s.supply_id
HAVING COALESCE(
		(SELECT quantity FROM inventory
		WHERE base_id = b.base_id AND supply_id = s.supply_id LIMIT 1),0) < 1
ORDER BY supply_id;

-- Which base earned the most from ticket sales

SELECT
	b.base_name,
	b.ticket_price,
	SUM(b.ticket_price) AS sum_price_ticket
FROM base b
LEFT JOIN martian m
	USING(base_id)
INNER JOIN visitor v
	ON m.martian_id = v.host_id
GROUP BY b.base_name,b.ticket_price
ORDER BY sum_price_ticket DESC
LIMIT 1;

-- The third most visited base

SELECT 
	b.base_name,
	COUNT(visitor_id) AS visitor 
FROM base b
LEFT JOIN martian m
	USING(base_id)
INNER JOIN visitor v
	ON m.martian_id = v.host_id
GROUP BY b.base_name
ORDER BY visitor DESC
OFFSET 2 LIMIT 1;









