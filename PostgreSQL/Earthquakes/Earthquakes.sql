-- PostgreSQL Portfolio Project 
-- Author: Novak Nozica Junior Data Analyst

-- This Dataset is about the earthquakes which has a magnitude of 6 or greater from 1900 - 2013
-- Source: United States Geological Survey (USGS)

-- Let's check magnitude range
SELECT 
	MAX(mag) AS max_magnitude,
	MIN(mag) AS min_magnitude
FROM earthquake;

-- Let's check depth range
SELECT 
	MAX(depth) AS max_depth,
	MIN(depth) AS min_depth
FROM earthquake;

-- Let's check date range
SELECT MAX(date),MIN(date)
FROM earthquake;

-- Let's check for missing data
SELECT *
FROM earthquake
WHERE mag IS NULL;

DELETE 
FROM earthquake
WHERE earthquake_id = 4596;


-- How many earthquakes of magnitude 6 or greater occurred between 1900 and 2013
SELECT 
	COUNT(*)
FROM earthquake;

-- Strongest earthquake between 1900 and 2013
SELECT *
FROM earthquake
WHERE mag = 
	(SELECT MAX(mag) FROM earthquake);
	
-- Top 10 strongest earthquakes between 1900 and 2013
SELECT *
FROM earthquake
ORDER BY mag DESC
LIMIT 10;

-- Updating the table
UPDATE earthquake
SET country = 'Japan'
WHERE country = 'Japanregion';

-- In which countries have the most earthquakes occurred
SELECT 
	country,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY country
ORDER BY number_of_quakes DESC;

-- How much of the epicenter of the earthquake is below the surface of the land,
-- and how much is below the surface of the sea
SELECT 
	COUNT(*) AS number_of_quakes,
	'below the surface of the land' AS epicenter
FROM earthquake
WHERE country IS NOT NULL
UNION
SELECT 
	COUNT(*) AS number_of_quakes,
	'below the surface of the see' AS epicenter
FROM earthquake
WHERE country IS NULL;

-- At what average depth is the epicenter of an earthquake
SELECT 
	ROUND(AVG(depth)) AS avg_depth
FROM earthquake;

-- At what average depth is the epicenter of an earthquake with a magnitude greater than 8
SELECT 
	ROUND(AVG(depth)) AS avg_depth
FROM earthquake
WHERE mag > 8;

-- In which year was the largest number of earthquakes recorded
SELECT 
	DATE_PART('YEAR', date) AS year,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY year
ORDER BY number_of_quakes DESC;

-- In which month was the largest number of earthquakes recorded
SELECT 
	DATE_PART('MONTH', date) AS month,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY month
ORDER BY number_of_quakes DESC;

-- At what time did the largest number of earthquakes occur
SELECT 
	EXTRACT(HOUR FROM time) AS hour,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY hour
ORDER BY number_of_quakes DESC;

-- What was the largest earthquake in 2006
SELECT * 
FROM earthquake
WHERE date >= '2006-01-01' AND date <= '2006-12-31'
ORDER BY mag DESC-- PostgreSQL Portfolio Project 
-- Author: Novak Nozica Junior Data Analyst

-- This Dataset is about the earthquakes which has a magnitude of 6 or greater from 1900 - 2013
-- Source: United States Geological Survey (USGS)

-- Let's check magnitude range
SELECT 
	MAX(mag) AS max_magnitude,
	MIN(mag) AS min_magnitude
FROM earthquake;

-- Let's check depth range
SELECT 
	MAX(depth) AS max_depth,
	MIN(depth) AS min_depth
FROM earthquake;

-- Let's check date range
SELECT MAX(date),MIN(date)
FROM earthquake;

-- Let's check for missing data
SELECT *
FROM earthquake
WHERE mag IS NULL;

DELETE 
FROM earthquake
WHERE earthquake_id = 4596;


-- How many earthquakes of magnitude 6 or greater occurred between 1900 and 2013
SELECT 
	COUNT(*)
FROM earthquake;

-- Strongest earthquake between 1900 and 2013
SELECT *
FROM earthquake
WHERE mag = 
	(SELECT MAX(mag) FROM earthquake);
	
-- Top 10 strongest earthquakes between 1900 and 2013
SELECT *
FROM earthquake
ORDER BY mag DESC
LIMIT 10;

-- Updating the table
UPDATE earthquake
SET country = 'Japan'
WHERE country = 'Japanregion';

-- In which countries have the most earthquakes occurred
SELECT 
	country,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY country
ORDER BY number_of_quakes DESC;

-- How much of the epicenter of the earthquake is below the surface of the land,
-- and how much is below the surface of the sea
SELECT 
	COUNT(*) AS number_of_quakes,
	'below the surface of the land' AS epicenter
FROM earthquake
WHERE country IS NOT NULL
UNION
SELECT 
	COUNT(*) AS number_of_quakes,
	'below the surface of the see' AS epicenter
FROM earthquake
WHERE country IS NULL;

-- At what average depth is the epicenter of an earthquake
SELECT 
	ROUND(AVG(depth)) AS avg_depth
FROM earthquake;

-- At what average depth is the epicenter of an earthquake with a magnitude greater than 8
SELECT 
	ROUND(AVG(depth)) AS avg_depth
FROM earthquake
WHERE mag > 8;

-- In which year was the largest number of earthquakes recorded
SELECT 
	DATE_PART('YEAR', date) AS year,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY year
ORDER BY number_of_quakes DESC;

-- In which month was the largest number of earthquakes recorded
SELECT 
	DATE_PART('MONTH', date) AS month,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY month
ORDER BY number_of_quakes DESC;

-- At what time did the largest number of earthquakes occur
SELECT 
	EXTRACT(HOUR FROM time) AS hour,
	COUNT(earthquake_id) AS number_of_quakes
FROM earthquake
GROUP BY hour
ORDER BY number_of_quakes DESC;

-- What was the largest earthquake in 2006
SELECT * 
FROM earthquake
WHERE date >= '2006-01-01' AND date <= '2006-12-31'
ORDER BY mag DESC
LIMIT 1;

