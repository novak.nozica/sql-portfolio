-- MySQL PORTFOLIO PROJECT 
-- Author: Novak Nozica Junior Data Analyst 

-- Task: Create a imaginary database Covid_hospital including next tables (employee, hospital, patient and examinations)

-- Table employee have records about personal data of medical stuff 
-- (employee_ID, first_name, last_name, sex, birth_date, hire_date, job_title, salary, hospital_ID)

-- Table hospital contains informations about five Covid_hospitals (KBC Zvezdara, KBC Dragisa Misovic, KBC Novi Sad, KBC Kragujevac and KBC Nis)
-- (hospital_ID, hospital_name, city, address, phone)

-- Patient table contains personal data about patients 
-- (patient_ID, first_name, last_name, sex, birth_date, comorbidities, hospital_ID)

-- Examinations table contains informations about the doctors who received the patients as well as diagnostic informations
-- (examinations_ID, employee_ID, patient_ID, receipt_date, diagnosis, release_date, status, comment)

-- Let's start codding 

CREATE DATABASE Covid_hospital;
USE Covid_hospital;

-- Creating tables

CREATE TABLE employee (
employee_ID INT PRIMARY KEY auto_increment,
first_Name VARCHAR (20),
last_Name VARCHAR (20),
sex VARCHAR (1),
birth_Date DATE,
hire_date DATE,
job_title VARCHAR (20),
salary INT NOT NULL,
hospital_ID INT NOT NULL
);

CREATE TABLE hospital (
hospital_ID INT PRIMARY KEY auto_increment,
hospital_name VARCHAR (20),
city VARCHAR (20),
address VARCHAR (40),
phone VARCHAR (20)
);

CREATE TABLE patient (
patient_ID INT PRIMARY KEY auto_increment,
first_name VARCHAR (20),
last_name VARCHAR (20),
sex VARCHAR (20),
birth_date DATE,
comorbidities VARCHAR(60),
hospital_ID INT NOT NULL
);

CREATE TABLE examinations (
examinations_ID INT PRIMARY KEY auto_increment,
employee_ID INT NOT NULL,
patient_ID INT NOT NULL,
receipt_date DATE,
diagnosis VARCHAR (40),
release_date DATE,
status VARCHAR (40),
comment TEXT
);

-- Adding foreign keys 

ALTER TABLE employee
ADD FOREIGN KEY (hospital_ID) REFERENCES hospital (hospital_ID); 

ALTER TABLE patient
ADD FOREIGN KEY (hospital_ID) REFERENCES hospital (hospital_ID); 

ALTER TABLE examinations 
ADD FOREIGN KEY (employee_ID) REFERENCES employee (employee_ID),
ADD FOREIGN KEY (patient_ID) REFERENCES patient (patient_ID);

-- Adding values to the tables

INSERT INTO employee
VALUES
(DEFAULT, 'Marko', 'Jovanovic', 'M', '1967-07-03', '1994-01-01', 'director', 247000, 1),
(DEFAULT, 'Nemanja', 'Milicevic', 'M', '1958-07-13', '1990-01-01', 'director', 248000, 2),
(DEFAULT, 'Jakov', 'Nemanjic', 'M', '1968-12-04', '1994-01-01', 'director', 229000, 3),
(DEFAULT, 'Marija', 'Mandic', 'F', '1970-02-21', '1999-01-01', 'director', 231000, 4),
(DEFAULT, 'Senka', 'Marjanovic', 'F', '1957-07-03', '1990-06-01', 'director', 249000, 5),
(DEFAULT, 'Mirko', 'Mirkovic', 'M', '1970-11-14', '2000-06-01', 'surgeon', 211000, 1),
(DEFAULT, 'Jovana', 'Marjanovic', 'F', '1974-12-12', '1998-06-01', 'surgeon', 196550, 1),
(DEFAULT, 'Nenad', 'Knezevic', 'M', '1974-01-12', '2004-06-01', 'anesthesiologist', 179500, 1),
(DEFAULT, 'Jovana', 'Peric', 'F', '1976-02-12', '2001-06-01', 'anesthesiologist', 187451, 1),
(DEFAULT, 'Jovan', 'Jovanovic', 'M', '1966-02-12', '1996-06-01', 'pulmonologist', 175400, 1),
(DEFAULT, 'Miljan', 'Mikic', 'M', '1969-02-12', '1997-01-01', 'pulmonologist', 169500, 1),
(DEFAULT, 'Miljan', 'Markovic', 'M', '1969-02-12', '1997-01-01', 'pulmonologist', 169500, 1),
(DEFAULT, 'Jovan', 'Mikic', 'M', '1974-02-12', '1999-01-01', 'infectologist', 136985, 1),
(DEFAULT, 'Dunja', 'Markovic', 'F', '1989-09-12', '2013-01-01', 'infectologist', 126985, 1),
(DEFAULT, 'Marina', 'Marinovic', 'F', '1989-11-04', '2012-01-01', 'nurse', 74500, 1),
(DEFAULT, 'Jelena', 'Jelenovic', 'F', '1987-06-06', '2010-01-01', 'nurse', 65000, 1),
(DEFAULT, 'Jelena', 'Markovic', 'F', '1968-02-17', '1994-01-01', 'nurse', 82000, 1),
(DEFAULT, 'Dunja', 'Markovic', 'F', '1987-08-25', '2017-01-01', 'nurse', 69000, 1),
(DEFAULT, 'Novak', 'Novakovic', 'M', '1984-03-29', '2013-06-01', 'nurse', 75000, 1),
(DEFAULT, 'Branko', 'Brankovic', 'M', '1980-11-14', '2000-06-01', 'surgeon', 201356, 2),
(DEFAULT, 'Milica', 'Milovanovic', 'F', '1954-12-12', '1998-06-01', 'surgeon', 198550, 2),
(DEFAULT, 'Novak', 'Novakovic', 'M', '1970-11-14', '2000-06-01', 'surgeon', 211000, 3),
(DEFAULT, 'Milica', 'Marjanovic', 'F', '1974-12-12', '1998-06-01', 'surgeon', 196550, 3),
(DEFAULT, 'Milos', 'Mirkovic', 'M', '1970-11-14', '2000-06-01', 'surgeon', 211000, 4),
(DEFAULT, 'Jelica', 'Marjanovic', 'F', '1974-12-12', '1998-06-01', 'surgeon', 196550, 4),
(DEFAULT, 'Marko', 'Marjanovic', 'M', '1970-11-14', '2000-06-01', 'surgeon', 213000, 5),
(DEFAULT, 'Milica', 'Jovanovic', 'F', '1974-12-12', '1998-06-01', 'surgeon', 196550, 5),
(DEFAULT, 'Novak', 'Knezevic', 'M', '1960-01-12', '2004-06-01', 'anesthesiologist', 179500, 2),
(DEFAULT, 'Mitar', 'Jovanovic', 'M', '1974-01-13', '2000-06-01', 'anesthesiologist', 179500, 3),
(DEFAULT, 'Mitar', 'Knezevic', 'M', '1979-01-12', '1998-06-01', 'anesthesiologist', 179500, 3),
(DEFAULT, 'Jovana', 'Jokic', 'F', '1976-02-12', '1999-06-01', 'anesthesiologist', 187451, 4),
(DEFAULT, 'Minja', 'Peric', 'F', '1986-08-17', '2001-06-01', 'anesthesiologist', 187451, 5),
(DEFAULT, 'Jovana', 'Mrkic', 'F', '1966-02-12', '1996-06-01', 'pulmonologist', 179400, 2),
(DEFAULT, 'David', 'Dragic', 'M', '1958-08-14', '1991-06-01', 'pulmonologist', 185400, 3),
(DEFAULT, 'Miljan', 'Draskovic', 'M', '1966-09-09', '1996-06-01', 'pulmonologist', 155400, 4),
(DEFAULT, 'Borislav', 'Jakic', 'M', '1966-11-12', '1996-06-01', 'pulmonologist', 185400, 5),
(DEFAULT, 'Novak', 'Mikic', 'M', '1974-04-12', '1999-01-01', 'infectologist', 146985, 2),
(DEFAULT, 'Daliborka', 'Budak', 'F', '1979-09-12', '2013-01-01', 'infectologist', 129985, 3),
(DEFAULT, 'Marko', 'Markovic', 'M', '1974-09-12', '1999-01-01', 'infectologist', 136985, 4),
(DEFAULT, 'Dijana', 'Milovanovic', 'F', '1969-12-12', '1998-01-01', 'infectologist', 126985, 5),
(DEFAULT, 'Ana', 'Marinovic', 'F', '1982-11-04', '2012-01-01', 'nurse', 74500, 2),
(DEFAULT, 'Ana', 'Jelenovic', 'F', '1988-06-06', '2010-01-01', 'nurse', 65000, 3),
(DEFAULT, 'Ljubica', 'Markovic', 'F', '1978-02-17', '1994-01-01', 'nurse', 82000, 4),
(DEFAULT, 'Dunja', 'Ljubisavljevic', 'F', '1989-08-25', '2017-01-01', 'nurse', 69000, 5),
(DEFAULT, 'Novak', 'Marinovic', 'M', '1983-03-29', '2013-06-01', 'nurse', 75000, 2),
(DEFAULT, 'Srna', 'Marinovic', 'F', '1989-11-04', '2012-01-01', 'nurse', 74500, 3),
(DEFAULT, 'Ljubica', 'Jelenovic', 'F', '1987-06-06', '2010-01-01', 'nurse', 65000, 3),
(DEFAULT, 'Magda', 'Markovic', 'F', '1968-02-17', '1994-01-01', 'nurse', 82000, 3),
(DEFAULT, 'Dunja', 'Sandic', 'F', '1987-08-25', '2017-01-01', 'nurse', 69000, 4),
(DEFAULT, 'Nikola', 'Novakovic', 'M', '1984-03-29', '2013-06-01', 'nurse', 75000, 4),
(DEFAULT, 'Marina', 'Sandulovic', 'F', '1989-11-04', '2012-01-01', 'nurse', 74500, 4),
(DEFAULT, 'Mira', 'Jelenovic', 'F', '1987-08-06', '2010-01-01', 'nurse', 65000, 5),
(DEFAULT, 'Jelena', 'Kravic', 'F', '1968-02-14', '1994-01-01', 'nurse', 82000, 5),
(DEFAULT, 'Jasna', 'Markovic', 'F', '1987-09-25', '2017-01-01', 'nurse', 69000, 5),
(DEFAULT, 'Bogdan', 'Novakovic', 'M', '1984-03-22', '2013-01-01', 'nurse', 75000, 5);


INSERT INTO hospital 
VALUES
(DEFAULT, 'KBC Zvezdara', 'Beograd', 'Dimitrija Tucovica 161', '011/221-413'),
(DEFAULT, 'KBC Dragisa Misovic', 'Beograd', 'Crnotravska 12', '011/123-369'),
(DEFAULT, 'KBC Novi Sad', 'Novi Sad', 'Futoska 89', '021/123-457'),
(DEFAULT, 'KBC Kragujevac', 'Kragujevac', 'Narodne Brigade 9', '034/784-584'),
(DEFAULT, 'KBC NIS', 'Nis', 'Bulevar Oslobodjenja 34', '018-791-382');


INSERT INTO patient
VALUES
(DEFAULT, "Zvezdan", "Jankovic", "M", "1965-04-23", NULL, 1),
(DEFAULT, "Jelena", "Birovljevic", "F", "1933-01-08", "diabetes", 1),
(DEFAULT, "Jelena", "Zelenovic", "F", "1933-06-18", "cancer", 1),
(DEFAULT, "Vladimir", "Putic", "M", "1976-01-17", "obesity", 1),
(DEFAULT, "Zarko", "Zarkovic", "M", "1945-08-09", "diabetes and hearth disease", 1),
(DEFAULT, "Nikola", "Radic", "M", "1989-02-04", NULL, 1),
(DEFAULT, "Marko", "Markovic", "M", "1993-10-12", "none" , 1),
(DEFAULT, "Svetlana", "Aleksic", "F", "1951-11-08", "cancer", 1),
(DEFAULT, "Marija", "Pantic", "F", "1978-07-08", "diabetes", 1),
(DEFAULT, "Mitar", "Blazic", "M", "1943-01-28", "hearth disease", 1),
(DEFAULT, "Dragan", "Jovanovic", "M", "1994-07-07", "none", 2),
(DEFAULT, "Jelena", "Stevanovic", "F", "1999-03-14", "none", 2),
(DEFAULT, "Pedja", "Peric", "M", "1964-01-27", "cancer", 2),
(DEFAULT, "Milutin", "Draskovic", "M", "1989-12-12", "diabetes", 2),
(DEFAULT, "Ksenija", "Pavlovic", "F", "1986-09-13", "pregnancy", 2),
(DEFAULT, "Pavle", "Obradovic", "M", "1976-08-11", "none", 2),
(DEFAULT, "Jovana", "Jelicic", "F", "1987-03-07", NULL, 2),
(DEFAULT, "Marija", "Jovanovic", "F", "1994-12-07", "none", 2),
(DEFAULT, "Nenad", "Nenadic", "M", "1957-12-17", "diabetes and cancer", 2),
(DEFAULT, "Katarina", "Milicic", "F", "2001-01-12", "pregnancy", 2),
(DEFAULT, "Novak", "Novakovic", "M", "1988-05-13", "none", 3),
(DEFAULT, "Petar", "Petrovic", "M", "1954-04-15", NULL, 3),
(DEFAULT, "Rada", "Radenkovic", "F", "1968-10-15", "obesity", 3),
(DEFAULT, "Marko", "Markovic", "M", "1974-03-18", "obesity", 3),
(DEFAULT, "Milica", "Miljkovic", "F", "1987-11-01", "pregnancy", 3),
(DEFAULT, "Jelica", "Stamatovic", "F", "1973-08-23", "none", 3),
(DEFAULT, "Jovan", "Jovanovic", "M", "1947-04-15", NULL, 3),
(DEFAULT, "Jovana", "Mirkovic", "F", "1966-08-25", "diabetes", 3),
(DEFAULT, "Marija", "Marinovic", "F", "1946-12-07", "hearth disease", 3),
(DEFAULT, "Luka", "Ivkovic", "M", "1952-01-26", "obesity, diabetes, hearth disease", 3),
(DEFAULT, "Sara", "Kasalovic", "F", "1981-06-08", "none", 3),
(DEFAULT, "Milos", "Milosevic", "M", "1966-04-29", "none", 3),
(DEFAULT, "Marija", "Jakic", "F", "1978-07-08", "diabetes", 4),
(DEFAULT, "Neven", "Jovanovic", "M", "1994-07-07", "none", 4),
(DEFAULT, "Bojana", "Stevanovic", "F", "1999-03-14", "none", 4),
(DEFAULT, "Bojan", "Peric", "M", "1964-01-27", "cancer", 4),
(DEFAULT, "Bozidar", "Draskovic", "M", "1989-12-12", "diabetes", 4),
(DEFAULT, "Ksenija", "Milicevic", "F", "1986-09-13", "pregnancy", 4),
(DEFAULT, "Pavle", "Pavlovic", "M", "1976-08-11", "none", 4),
(DEFAULT, "Jovana", "Jokic", "F", "1987-03-07", NULL, 4),
(DEFAULT, "Marija", "Milutinovic", "F", "1994-12-07", "none", 4),
(DEFAULT, "Novak", "Nenadic", "M", "1957-12-17", "diabetes and cancer", 4),
(DEFAULT, "Jana", "Milicic", "F", "2001-01-12", "pregnancy", 4),
(DEFAULT, "Nemanja", "Novakovic", "M", "1988-05-13", "none", 4),
(DEFAULT, "Petar", "Piric", "M", "1954-04-15", NULL, 4),
(DEFAULT, "Senka", "Radenkovic", "F", "1968-10-15", "obesity", 4),
(DEFAULT, "Rada", "Mikic", "F", "1968-10-15", "obesity", 5),
(DEFAULT, "Jovan", "Markovic", "M", "1974-03-18", "obesity", 5),
(DEFAULT, "Milica", "Sikimic", "F", "1987-11-01", "pregnancy", 5),
(DEFAULT, "Dunja", "Stamatovic", "F", "1973-08-23", "none", 5),
(DEFAULT, "Nikola", "Jovanovic", "M", "1947-04-15", NULL, 5),
(DEFAULT, "Jovana", "Smiljkovic", "F", "1966-08-25", "diabetes", 5),
(DEFAULT, "Srna", "Marinovic", "F", "1946-12-07", "hearth disease", 5),
(DEFAULT, "Luka", "Kukic", "M", "1952-01-26", "obesity, diabetes, hearth disease", 5),
(DEFAULT, "Sara", "Lukic", "F", "1981-06-08", NULL, 5),
(DEFAULT, "Sinisa", "Jankovic", "M", "1965-04-23", NULL, 5),
(DEFAULT, "Jelena", "Katic", "F", "1933-01-08", "diabetes", 5),
(DEFAULT, "Marina", "Zelenovic", "F", "1933-06-18", "cancer", 5),
(DEFAULT, "Milos", "Putic", "M", "1976-01-17", "obesity", 5),
(DEFAULT, "Zarko", "Zakic", "M", "1945-08-09", "diabetes and hearth disease", 5),
(DEFAULT, "Nikola", "Jelikic", "M", "1989-02-04", NULL, 5),
(DEFAULT, "Mitar", "Markovic", "M", "1993-10-12", "none" , 5),
(DEFAULT, "Svetlana", "Lekic", "F", "1951-11-08", "cancer", 5),
(DEFAULT, "Marija", "Milojkovic", "F", "1978-07-08", "diabetes", 5),
(DEFAULT, "Jakov", "Blazic", "M", "1943-01-28", "hearth disease", 5),
(DEFAULT, "Nenad", "Jovanovic", "M", "1994-07-07", NULL, 5),
(DEFAULT, "Sanja", "Stevanovic", "F", "1999-03-14", "none", 5),
(DEFAULT, "Predrag", "Peric", "M", "1964-01-27", "cancer", 5),
(DEFAULT, "Milutin", "Milutinovic", "M", "1989-12-12", "diabetes", 5),
(DEFAULT, "Ksenija", "Jaksic", "F", "1986-09-13", "pregnancy", 5),
(DEFAULT, "Pavle", "Pavlovic", "M", "1976-08-11", "none", 5),
(DEFAULT, "Jovana", "Joksimovic", "F", "1987-03-07", NULL, 5),
(DEFAULT, "Nina", "Jovanovic", "F", "1994-12-07", "none", 5),
(DEFAULT, "Nenad", "Nenadovic", "M", "1957-12-17", "diabetes and cancer", 5);


INSERT INTO examinations
VALUES 
(DEFAULT, 38, 1, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 34, 2, "2021-11-05", "covid_positive", "2021-11-15", "died",NULL),
(DEFAULT, 39, 3, "2021-11-05", "covid_positive", "2021-11-17", "died",NULL),
(DEFAULT, 35, 4, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 35, 5, "2021-11-05", "covid_positive", "2021-12-22", "reliesed",NULL),
(DEFAULT, 35, 6, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 39, 7, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 35, 8, "2021-11-06", "covid_positive", "2021-11-27", "died",NULL),
(DEFAULT, 35, 9, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 39, 10, "2021-11-06", "covid_positive", "2021-12-19", "reliesed",NULL),
(DEFAULT, 37, 11, "2021-11-05", "covid_positive", "2021-11-05", "reliesed",NULL),
(DEFAULT, 37, 12, "2021-11-05", "covid_positive", "2021-11-05", "reliesed",NULL),
(DEFAULT, 33, 13, "2021-11-05", "covid_positive", "2021-11-30", "died",NULL),
(DEFAULT, 33, 14, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 34, 15, "2021-11-05", "covid_positive", "2021-11-25", "reliesed",NULL),
(DEFAULT, 38, 16, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 38, 17, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 34, 18, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 38, 19, "2021-11-06", "covid_positive", "2021-11-19", "died",NULL),
(DEFAULT, 34, 20, "2021-11-06", "covid_positive", "2021-11-25", "reliesed",NULL),
(DEFAULT, 36, 21, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 14, 22, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 14, 23, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 13, 24, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 14, 25, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 14, 26, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 13, 27, "2021-11-05", "covid_positive", "2021-11-21", "reliesed",NULL),
(DEFAULT, 14, 28, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 13, 29, "2021-11-06", "covid_positive", "2021-11-27", "died",NULL),
(DEFAULT, 37, 30, "2021-11-06", "covid_positive", "2021-11-21", "died",NULL),
(DEFAULT, 37, 31, "2021-11-06", "covid_positive", "2021-11-12", "reliesed",NULL),
(DEFAULT, 37, 32, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 37, 33, "2021-11-05", "covid_positive", "2021-11-05", "reliesed",NULL),
(DEFAULT, 37, 34, "2021-11-05", "covid_positive", "2021-11-05", "reliesed",NULL),
(DEFAULT, 33, 35, "2021-11-05", "covid_positive", "2021-11-30", "died",NULL),
(DEFAULT, 33, 36, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 40, 37, "2021-11-05", "covid_positive", "2021-11-25", "reliesed",NULL),
(DEFAULT, 38, 38, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 38, 39, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 34, 40, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 38, 41, "2021-11-06", "covid_positive", "2021-11-19", "died",NULL),
(DEFAULT, 34, 42, "2021-11-06", "covid_positive", "2021-11-25", "reliesed",NULL),
(DEFAULT, 38, 43, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 36, 44, "2021-11-05", "covid_positive", "2021-11-15", "died",NULL),
(DEFAULT, 39, 45, "2021-11-05", "covid_positive", "2021-11-17", "died",NULL),
(DEFAULT, 35, 46, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 40, 46, "2021-11-05", "covid_positive", "2021-12-22", "reliesed",NULL),
(DEFAULT, 35, 47, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 39, 48, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 35, 49, "2021-11-06", "covid_positive", "2021-11-27", "died",NULL),
(DEFAULT, 35, 50, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 39, 51, "2021-11-06", "covid_positive", "2021-12-19", "reliesed",NULL),
(DEFAULT, 13, 52, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 14, 53, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 14, 54, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 13, 55, "2021-11-05", "covid_positive", "2021-11-21", "reliesed",NULL),
(DEFAULT, 14, 56, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 13, 57, "2021-11-06", "covid_positive", "2021-11-27", "died",NULL),
(DEFAULT, 37, 58, "2021-11-06", "covid_positive", "2021-11-21", "died",NULL),
(DEFAULT, 37, 59, "2021-11-06", "covid_positive", "2021-11-12", "reliesed",NULL),
(DEFAULT, 37, 60, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 38, 61, "2021-11-05", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 34, 62, "2021-11-05", "covid_positive", "2021-11-15", "died",NULL),
(DEFAULT, 36, 63, "2021-11-05", "covid_positive", "2021-11-17", "died",NULL),
(DEFAULT, 35, 64, "2021-11-05", "covid_negative", "2021-11-05", "reliesed",NULL),
(DEFAULT, 35, 65, "2021-11-05", "covid_positive", "2021-12-22", "reliesed",NULL),
(DEFAULT, 35, 66, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 39, 67, "2021-11-06", "covid_negative", "2021-11-06", "reliesed",NULL),
(DEFAULT, 40, 68, "2021-11-06", "covid_positive", "2021-11-27", "died",NULL),
(DEFAULT, 36, 69, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 36, 70, "2021-11-06", "covid_positive", "2021-12-19", "reliesed",NULL),
(DEFAULT, 40, 71, "2021-11-05", "covid_positive", "2021-11-05", "reliesed",NULL),
(DEFAULT, 35, 72, "2021-11-06", "covid_positive", "2021-11-15", "reliesed",NULL),
(DEFAULT, 40, 73, "2021-11-06", "covid_positive", "2021-12-19", "reliesed",NULL),
(DEFAULT, 40, 74, "2021-11-05", "covid_positive", "2021-11-05", "reliesed",NULL);

-- Preparing data for analysis 

-- Checking for missing values

SELECT *
FROM patient
WHERE comorbidities IS NULL;

-- Let's see how many patients have missing values about their comorbidities

SELECT 
	COUNT(patient_ID) AS number_of_patients,
    comorbidities
FROM patient
WHERE comorbidities IS NULL;

-- There are 13 missing values and after doing some research we update comorbidities column 

UPDATE patient
SET comorbidities = 'none'
WHERE patient_ID IN (1,17,22,27,40,45,51,55,56,61,66); 

UPDATE patient
SET comorbidities = 'hearth disease'
WHERE patient_ID = 6;

UPDATE patient
SET comorbidities = 'diabetes, obesity'
WHERE patient_ID = 72;

-- There are no more missing values! 

-- Let's see is there any duplicated row in employee table

SELECT 
	*,
    COUNT(employee_ID)
FROM employee
GROUP BY employee_ID
HAVING COUNT(employee_ID) > 1;

-- There are no duplicated entries 

-- Employee with employee_ID 53 quit his job and we want to update employee table 

DELETE
FROM employee
WHERE employee_ID = 53;

-- The data is cleaned and we can proced with data analysis answering next questions:

-- How many employees work in each hospital?
SELECT 
	h.hospital_name,
	COUNT(employee_ID) AS employee_number
FROM employee e
INNER JOIN hospital h
	ON e.hospital_ID = h.hospital_ID
GROUP BY hospital_name;

-- How many men and how many women work in each hospital?
SELECT 
	sex,
    hospital_name,
    COUNT(employee_ID) AS employee_number
FROM employee 
JOIN hospital
	USING(hospital_ID)
GROUP BY hospital_name, sex;

-- Who has higher average earnings - men or women?
SELECT 
	ROUND(AVG(salary)) AS avg_salary,
    sex
FROM employee
WHERE sex = 'M'
UNION
SELECT 
	ROUND(AVG(salary)) AS avg_salary,
    sex
FROM employee
WHERE sex = 'F';


-- Return top 5 employees with the most work experience?
SELECT 
	CONCAT(first_name,' ',last_name) AS employee,
    hire_date
FROM employee
ORDER BY hire_date
LIMIT 5;

-- Return all male employees who have more than 25 years of experience?
SELECT 
	first_name,
    last_name,
    YEAR(hire_date) AS started_working
FROM employee
WHERE sex = 'M'
HAVING started_working < 1998;

-- Employees with the lowest and highest salary?
SELECT * 
FROM employee
WHERE salary = 
	(SELECT MAX(salary) FROM employee)
UNION 
SELECT * 
FROM employee
WHERE salary = 
	(SELECT MIN(salary) FROM employee);

-- Return average salaries by job title?
SELECT 
	job_title AS job,
    ROUND(AVG(salary)) AS avg_salary
FROM employee
GROUP BY job_title;

-- Return total hospital issuance for employee salaries?
SELECT 
	hospital_name,
    SUM(salary) AS total_salary,
    COUNT(employee_ID) AS employee_number,
	ROUND(SUM(salary)/COUNT(employee_ID)) AS salary_per_employee
FROM employee
JOIN hospital
	USING(hospital_ID)
GROUP BY hospital_name
ORDER BY total_salary DESC;

-- In which hospital are surgeons the most paid?
SELECT 
	hospital_name,
    job_title,
    ROUND(AVG(salary)) AS avg_salary
FROM hospital
JOIN employee
	USING(hospital_ID)
WHERE job_title = 'surgeon'
GROUP BY hospital_name
ORDER BY salary DESC;

-- Return all employess that are not working as surgeon?
SELECT *
FROM employee
WHERE job_title != 'surgeon';

-- Who is the third highest paid pulmonologist and where does he work?
SELECT 
	first_name,
    last_name,
    salary,
    hospital_name
FROM employee
JOIN hospital
	USING(hospital_ID)
WHERE job_title = 'pulmonologist'
ORDER BY salary DESC
LIMIT 2,1;

-- 5% salary increase for pulmonologists?
SELECT 
	first_name,
    last_name,
    job_title,
    salary,
    ROUND(salary * 1.05) AS salary_after_increase
FROM employee
WHERE job_title = 'pulmonologist';

-- Return employees in KBC Kragujevac with a salary range between 75000 and 175000?
SELECT *
FROM employee e
JOIN hospital h
	ON e.hospital_ID = h.hospital_ID
WHERE salary BETWEEN 75000 AND 175000 AND hospital_name = 'KBC Kragujevac';

-- Find patients whose name starts with the letter m?
SELECT *
FROM patient
WHERE first_name LIKE 'm%';

-- What are the most common comorbidities?
SELECT 
	comorbidities,
	COUNT(comorbidities) AS frequency
FROM patient
GROUP BY comorbidities
ORDER BY frequency DESC;

-- The oldest and the youngest patient?
SELECT
	first_name,
	last_name,
    birth_date
FROM patient
ORDER BY birth_date
LIMIT 1;

SELECT 
	first_name,
    last_name,
    birth_date
FROM patient
ORDER BY birth_date DESC
LIMIT 1;

-- How many covid positive and how many covid negative patients?
SELECT 
	diagnosis,
    COUNT(p.patient_ID) AS patient_number
FROM patient p
INNER JOIN examinations e
	ON p.patient_ID = e.patient_ID
GROUP BY diagnosis;

-- How many patients died (male vs female)?
SELECT 
	COUNT(p.patient_ID) AS patient_number,
    e.status,
    sex
FROM patient p
INNER JOIN examinations e
	ON p.patient_ID = e.patient_ID
WHERE status = 'died'
GROUP BY sex;

-- The youngest deceased patient?
SELECT 
	CONCAT(first_name,' ', last_name) AS patient,
    sex,
    birth_date,
    comorbidities,
    status
FROM patient p
INNER JOIN examinations e
	ON p.patient_ID = e.patient_ID
WHERE status = 'died'
ORDER BY birth_date DESC
LIMIT 1;

-- Are there any patients who died without comorbidities?
SELECT 
	CONCAT(first_name,' ', last_name) AS patient,
    sex,
    birth_date,
    comorbidities,
    status
FROM patient p
INNER JOIN examinations e
	ON p.patient_ID = e.patient_ID
WHERE status = 'died' AND comorbidities = 'none';

-- In which hospital did the most people die?
SELECT 
	h.hospital_name,
    COUNT(p.patient_ID) AS died_patient,
    e.status
FROM hospital h
JOIN patient p
	USING(hospital_ID)
JOIN examinations e
	USING(patient_ID)
WHERE status = 'died'
GROUP BY hospital_name;

-- Create a view of patients in KBC Nis?
CREATE VIEW KBC_Nis_patient AS
SELECT 
	h.hospital_name,
    p.patient_ID,
    p.first_name,
    p.last_name,
    p.sex,
    p.birth_date,
    p.comorbidities,
    e.receipt_date,
    e.diagnosis,
    e.release_date,
    e.status
FROM hospital h
JOIN patient p
	USING(hospital_ID)
JOIN examinations e
	USING(patient_ID)
WHERE h.hospital_name = 'KBC NIS';

-- Return the risk of death to patients based on their age and the presence of comorbidities?
SELECT 
CASE
	WHEN birth_date < '1940-01-01' OR comorbidities = 'cancer' THEN 'severe_risk'
    WHEN birth_date BETWEEN '1940-01-01' AND '1960-01-01' OR comorbidities IN ('diabetes', 'hearth_disease') THEN 'moderate_risk'
    WHEN birth_date BETWEEN '1960-01-01' AND '1980-01-01' OR comorbidities IN ('pregnancy', 'obesity') THEN 'slight_risk'
    WHEN birth_date > '1980-01-01' OR comorbidities = 'none' THEN 'low_risk'
END 'risk_range',
COUNT(patient_ID) AS died_patients
FROM patient
JOIN examinations
	USING(patient_ID)
WHERE status = 'died'
GROUP BY risk_range
ORDER BY died_patients DESC;

-- Return each hospital that have more than 12 patients?
SELECT
	hospital_name,
    COUNT(patient_ID) AS patient
FROM hospital
JOIN patient
	USING(hospital_ID)
GROUP BY hospital_name
HAVING patient > 12;

-- Create procedure? 
DELIMITER $$
CREATE PROCEDURE died_vs_released()
BEGIN
	SELECT 
	COUNT(p.patient_ID) AS patient,
    e.status
FROM patient p
JOIN examinations e
	ON p.patient_ID = e.patient_ID
GROUP BY status;
END $$
DELIMITER ;

CALL died_vs_released();

-- Top 3 employees with the most medical examinations of patients?
SELECT 
	em.first_name,
    em.last_name,
    em.job_title,
    ho.hospital_name,
    COUNT(ex.examinations_ID) AS number_of_examinations
FROM employee em
JOIN examinations ex
	USING(employee_ID)
JOIN hospital ho
	USING(hospital_ID)
GROUP BY employee_ID
ORDER BY number_of_examinations DESC
LIMIT 3;

-- How long, on average, do patients stay in the hospital for treatment(released vs died patients)
SELECT
	status,
    ROUND(AVG(release_date - receipt_date)) AS avg_days
FROM patient 
JOIN examinations
	USING(patient_ID)
WHERE diagnosis = 'covid_positive'
GROUP BY status;

-- Join all 4 tables together?
CREATE VIEW covid_hospital AS
SELECT 
	h.hospital_name,
    CONCAT(p.first_name, ' ',p.last_name) AS patient,
    p.birth_date,
    p.comorbidities,
    ex.receipt_date,
    ex.diagnosis,
    ex.release_date,
    ex.status,
    CONCAT(em.first_name,' ',em.last_name) AS employee,
    em.job_title,
    em.salary
FROM hospital h
RIGHT JOIN patient p
	USING(hospital_ID)
INNER JOIN examinations ex
	USING(patient_ID)
INNER JOIN employee em
	USING(employee_ID);
    
-- Every data tells a story even if it's completely fake. These are my insights from the given data: 

-- Hospitals are fully staffed and fighting the pandemic at full capacity;
-- The vast majority of employees have extensive work experience;
-- Directors, surgeons and anesthesiologists have the highest average salaries;
-- Of all the hospitals, KBC Zvezdara spends the most on employee salaries;
-- On average, women earn significantly less than men. This difference is certainly influenced by the fact that a large number of women work as nurses, who generally have the lowest salaries;
-- The most common comorbidities are diabetes, obesity and cancer;
-- 76% of people who are tested are covid positive;
-- There is no significant difference between the death ratio based on sex;
-- People who were born before 1940-01-01 or who have cancer have the highest risk of death - 47% off all died people;
-- What is very worrying is that persons who were born after 1980-01-01 and who do not have any comorbidities participate in the death rate of up to 23%. This can be explained by the fact that these people feel completely healthy, which causes them to report to the doctor later when the disease is already advanced;
-- The youngest deceased patient was only 24 years old;
-- The largest number of patients died in KBC Nis - 40% of all deceased patients;
-- On average, a covid-positive person who recovers spends about 4 weeks in the hospital;
-- On average, people who die from covid stay in the hospital for 2 weeks;

-- The next step in the data analysis process is data visualization and explanation the insights to stakeholders;

-- Thank you for your time :)

-- Novak Nozica Junior Data Analyst






    













